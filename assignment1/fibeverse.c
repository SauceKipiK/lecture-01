#include <stdlib.h>
#include <string.h>
#include "fibonacci.h"
#include "reverse.h"

int i = 1;

int main(int argc, char *argv[]) {

#ifdef FIBONACCI
if (i < argc) {
		print_fib(atoi(argv[i]));
		i++;
	}
#endif

#ifdef REVERSE
if (i < argc) {
		reverse(argv[i], strlen(argv[i]));
		i++;
	}
#endif

	return 0;
}