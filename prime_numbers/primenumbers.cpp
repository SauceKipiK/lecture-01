#include <iostream>
#include <math.h>

using namespace std;

int nbFacteur(int n) {
    int count = 2;
    int i;
    double racine = sqrt(n);

    for (i=2; i <= racine; i++)
    {
        if(n % i == 0)
        {
            count++;
        }
    }
return count;
}

int nbPremier(int n) {
    return(nbFacteur(n)==2);
}

int main() {

    int number;
    int i;

    cout << "Enter a number : ";
    cin >> number;

    cout << "List of prime numbers before " << number << " : ";
    for(i=2; i < number; i++)
    {
        if(nbPremier(i))
        {
            cout << i << " ";
        }
    }
    cout << ".\n";
}