# **Lecture 01**


## **Introduction**

The primary goal of this lab period is to get your C compiler and debugger (GCC.GDB) up and running using the Visual Studio Code IDE in a linux environment (WSL2).

We have 2 “Hello, World!” examples for you to practice compiling to make sure that everything is working.

## **Setup**

<details open>
<summary> Instructions:</summary>

* If you are not using Linux, WSL2 (Windows Subsystem for Linux) can be installed on Windows 10/11 following the documentation [here](https://learn.microsoft.com/en-us/windows/wsl/setup/environment). A Ubuntu distro is recommended.

* Install Visual Studio Code following the documentation [here](https://learn.microsoft.com/en-us/windows/wsl/tutorials/wsl-vscode). Do not forget to install the [Remote Development Extension Pack](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack). This allows VS Code to connect to your WSL2  machine, edit code and use GCC/GDB in Ubuntu Linux while allowing you to use the IDE in a Windows environment.

* Set up your Linux environment for C++ development following the documentation [here](https://code.visualstudio.com/docs/cpp/config-linux) and [here](https://code.visualstudio.com/docs/cpp/config-wsl).

### **Test Programs hello1, hello2**
<details>
<summary><b>hello1</b></summary>

The command we need to compile hello1 is:
```
gcc -Wall -Werror -Wextra -pedantic -std=c99 -g hello.c -o hello1
```

Take a look inside the .vscode folder. The tasks.json file contains the 'cppbuild' task which tells VS Code how to build our program. 
The launch.json file contains launch configuration which tells VS Code which file to execute, which debugger to use, the path of the debugger executable, and other details. Launch parameters used are documented [here](https://code.visualstudio.com/docs/cpp/launch-json-reference).

Press <F5> to start the program with the debugger connected. Execution will break at the first line. Press <F5> again to continue execution, and check the output in the terminal window (Ctrl + `).
</details>

<details>
<summary><b>hello2</b></summary>

The command we need to compile hello2 is:
```
gcc main.c hello.c -o hello2
```
 Set up the task.json to get VS Code to execute the above command to build hello2.
</details>
</details>

----------
## **Code Exercise:** Basic Statistics

<details>
<summary>Given a list of N integers, find its mean (as a double), maximum value, minimum value, and range. Your program will first ask for N, the number of integers in the list, which the user will input. Then the user will input N more numbers.</summary>
<br>

Here is a sample input sequence:
```
Enter <N>: 3
Enter numbers:
2
1
3
```
Three numbers are given: 2, 1, 3. The output should be as follows:
```
Mean: 2
Max: 3
Min: 1
Range: 2
```
</details>


----------
## **Code Exercise:** Prime Numbers
Write a program to read a number N from the user and then find the first N primes. A
prime number is a number that only has two divisors, one and itself.

----------
## **Paper Exercise:** What does it do?!
<details>
<summary>Do these problems without using a computer.</summary>
<br>

### 1. What does this snippet do?
Try doing a few examples with small numbers on paper if you’re stuck.
```
// bob and dole are integers
int accumulator = 0;                // Initialization of the variable accumulator to 0
while (true )                       // As long as it's true, we enter the loop while
{
    if (dole == 0) break ;            // If the variable dole equals 0, we break the loop and exit
    accumulator += (( dole % 2 == 1) ? bob : 0); // If dole modulo 2 equals 1, true : return bob,
                                                 // false : return 0. So afetr we do,
                                                 // accumulator = accumulator + (Either bob or 0)
    dole /= 2;                      // dole = dole / 2
    bob *= 2;                       // bob = bob * 2
}
cout << accumulator << "\n";          // Display the value of the variable accumulator
```

### 2. What does this program do?
What would the operating system assume about the
program’s execution?
```
#define O 1 // That ’s a 'OH' , not a zero
int main ()
{                                           // Now whenever the letter O is used in the program
    return O;                               // it is replaced by the number 1 before compiling.
}
```

### 3. What does this program do?
```
// N is a nonnegative integer
double acc = 0;                     // Initialization of the variable acc to 0
for (int i = 1; i <= N; ++i)    // For a variable i equal to 1(initializes variables and is executed 
{                               // only once), if i is less than or equal to N (a
                                // nonnegative int)(condition to enter the loop),
                                // i is incremented by 1 (updates the value of
                                // initialized variables and again checks the condition)
    double term = (1.0/i);          // Initialization of the variable term to (1.0/i)
    acc += term * term;             //acc = acc + (term * term)
    for (int j = 1; j < i; ++j) // For a variable j equal to 1 (initializes variables and is executed
    {                           // only once), if j is less than i (condition to enter
                                // the loop), j is incremented by 1 (updates the value of
                                // initialized variables and again checks the condition)
        acc *= -1;              // acc = acc * -1
    }
}
cout << acc << "\n";            // Display the value of the variable acc
```
</details>

----------
## **Code Exercise:** Factorial

<details>
<summary>This section focuses on debugging programs.</summary>
<br>

We will start off with a simple factorial program and do a step by step troubleshooting routine.

The code for the factorial program is located in this repo [here](https://gitlab.com/course-cpp-for-gamedev/lecture-01/-/tree/main/factorial). Set it up in your IDE and verify that it compiles.

What values do you get for the inputs: 0, 1, 2, 9, 10?

### **Breaking the Program**

Run the program and enter −1. What happens? How can you change the code such that it won’t exhibit this behavior?

*To avoid this, we will use the while loop, while as the variable number is less than 0, the user will have to re-enter a number.*

### **Breaking the Program II**

Try entering some larger numbers. What is the minimum number for which your modified program from 4.2 stops working properly? (You shouldn’t have to go past about 25. You may find Google’s built-in calculator useful in checking factorials.) Can you explain what has happened?

*The minimum number that gives a correct answer is 12.*
*This is because of the int type of the variable accumulator, instead of int we will replace with long int or long long int.*
*What differentiates the int, the long int and the long long int is the number of guaranteed minimum bits.*
*Long long takes the double memory as compared to int.*

### **Rewriting Factorial**

Modify the given code such that negative inputs do not break the program and the smallest
number that broke the program before now works. Do not hardcode answers.

### **Recursion**

[Recursion](https://en.wikipedia.org/wiki/Recursion_(computer_science)) is when functions themselves from within their own code. Can you rewrite the program to calculate the value recursively?

</details>

----------
## **Code Exercise:** Fibverse

<details>
<summary>Details</summary>
<br>
The assignment has 3 .c files:
* fibeverse.c
* fibonacci.c
* reverse.c
  
And two header files:
* fibonacci.h
* reverse.h

You can compile them with this command (though it won’t work at first; see Problem 1):
```
gcc -Wall -std=c99 _fibeverse.c reverse.c fibonacci.c_ -o fibeverse
```
You can run the resulting program with two arguments: a number, then a string (in quotes):
```
./fibeverse 6 'what a trip that was!'  8  was! that trip a what
```
The first line it prints is the 6th fibonacci number. The second line is the string you provided, with the words reversed.

### Problem 1

Unfortunately, the code doesn’t compile as-is! Fix the compile errors and warnings. gcc should produce no output with the above command when you are done.

### Problem 2

I can’t decide whether I want a program that computes Fibonacci numbers or a program that reverses strings! Let’s modify fibeverse so that it can be compiled into either.

Use the preprocessor macros we taught in class to make it so that I can choose which program it is at compile time.

When I compile it with this command, it should compute the Fibonacci number but not reverse the second argument:
```
gcc -Wall -std=c99 -DFIBONACCI fibeverse.c reverse.c fibonacci.c -o fibonacci
```

Then I can run it like this:
```
./fibonacci 8
```

When I use this command, it should reverse the string I provide as the first argument, and not do any fibonacci calculation:
```
gcc -Wall -std=c99 -DREVERSE fibeverse.c reverse.c fibonacci.c -o reverse
```

Then I can run it like this:
```
./reverse 'a brave new world'
```

It should work as it originally did when I provide both compiler flags:
```
gcc -Wall -std=c99 -DFIBONACCI -DREVERSE fibeverse.c reverse.c fibonacci.c -o fibeverse
```
</details>
