#include <iostream>

using namespace std;

long long int factorial(int);

int main ()
{
    short number ;
    cout << " Enter a number : ";
    cin >> number ;

    while (number < 0)
    {
        cout << " Enter a POSITIVE number : ";
        cin >> number ;
    }
    
    long long int accumulator = 1;
    accumulator = factorial(number);
    cout << "The factorial of " << number << " is " << accumulator << ".\n";
    
    return 0;
}


long long int factorial(int n){
    if (n > 1) {
        return n * factorial(n - 1);
    } else {
        return 1;
    }
}