#include <iostream>

using namespace std;

double mean(double tab[], int size) {
    double sum;
    double moyenne = 0.0;
    for (int i = 0; i < size; ++i)
    {
        sum += tab[i];
    }
    moyenne = sum / size;
    return moyenne;
}

double max(double tab[], int size) {
    double maximum = tab[0];
    for (int i = 0; i < size; i++)
    {
        if (maximum < tab[i])
        {
            maximum = tab[i];
        }
    }
    return maximum;
}

double min(double tab[], int size) {
    double minimum = tab[0];
    for (int i = 0; i < size; i++)
    {
        if (minimum > tab[i])
        {
            minimum = tab[i];
        }
    }
    return minimum;
}

int main() {
    int nbInList;
    cout << "Enter the numbers of data : ";
    cin >> nbInList;
    while (nbInList <= 0)
    {
        cout << "Error! number must be greater than 0." << endl;
        cout << "Enter the number again : ";
        cin >> nbInList;
    }
    double tabNb[nbInList];
    cout << "Enter numbers :" << endl;
    for (int i = 0; i < nbInList; i++)
    {
        cin >> tabNb[i];
    }
    cout << "Mean : " << mean(tabNb, nbInList) << endl;
    cout << "Min : " << min(tabNb, nbInList) << endl;
    cout << "Max : " << max(tabNb, nbInList) << endl;
    cout << "Range : " << max(tabNb, nbInList) - min(tabNb, nbInList) << endl;
}